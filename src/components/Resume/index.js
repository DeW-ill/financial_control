import React from "react";
import ResumeItem from "../ResumeItem";
import { Container } from "./styles";

import {
  FaRegArrowAltCircleUp,
  FaRegArrowAltCircleDown,
  FaDollarSign,
} from "react-icons/fa";
const Resume = () => {
  return (
    <Container>
      <ResumeItem
        title="Entradas"
        Icon={FaRegArrowAltCircleUp}
        currency="R$ "
        value="8000"
      />
      <ResumeItem
        title="Saídas"
        Icon={FaRegArrowAltCircleDown}
        currency="R$ "
        value="3000"
      />
      <ResumeItem
        title="Total"
        Icon={FaDollarSign}
        currency="R$ "
        value="5000"
      />
    </Container>
  );
};

export default Resume;
